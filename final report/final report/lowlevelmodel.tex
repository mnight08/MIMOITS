\section{Low Level Model and Reconstruction}
Here we give a detailed description of the mathematics that make RADAR imaging work.  First we describe the math behind the physics of RADAR.  This is called our forward model.  Next we bring in Multiple Input Multiple Output - (MIMO) RADAR.  Finally we focus on using our model to recover information about targets in our scene.  As stated when detailing the scope of the project, this model focuses on stationary targets only.


\subsection{Forward Model}

\subsubsection{The Wave Equation}
The wave equation for $u:\R^d \times \R \to \C$ in free space is given by
\begin{equation}
\left(\laplacian-c_0^{-2}\partial_t^{2}\right)u(x,t)=-j(x,t).
\end{equation}
The Green's function $g_0(x,t)$ for the wave equation in $3$ spatial dimensions given by
\begin{equation}g_0(x,t)=\frac{\delta(t-\frac{|x|}{c_0})}{4 \pi |x|}\end{equation}
can be used to solve for the wave $u(x,t)$ as
\begin{equation}
u(x,t)=\int_{\R^3}\int_{\R}g_0(x-z,t-\tau)  j(z,\tau) \dd \tau \dd z
\end{equation}


\subsubsection{Scalar Wave Model}
Under certain assumptions each component of the electric and magnetic vector fields governed by Maxwell's equations can be modeled by the wave equation.
\begin{itemize}
\item We adopt this as our model for the propagation of the scalar electromagnetic wave $u(x,t)$ in $3$-d space with source $j(x,t)$:   \begin{equation} \left(\laplacian - c^{-2}(x)\partial_t^2\right)u(x,t)=-j(x,t). \end{equation}
\item The total field $u(x,t)$ at any position in space $x$ and any instance in time $t$  is decomposed into incident and scattered fields  \begin{equation} u(x,t)=\uin(x,t)+\usc(x,t).\end{equation}
\item The incident field is in the absence of scatterers and satisfies  \begin{equation}  \left(\laplacian-c_0^{-2}\partial_t^2\right)\uin(x,t)=-j(x,t). \end{equation}
\end{itemize}


\subsubsection{Reflectivity function}

Define the \textbf{reflectivity} function $v(x)$ as
\begin{equation} \label{eq: ref}
v(x)=\frac{1}{c_0^2}-\frac{1}{c^2(x)}
\end{equation}
\begin{itemize}
\item This function tells us how EM radiation behaves at a point in space compared to in free space.
\item $v(x)$ is $0$ in the absence of targets.
\item In the presence of targets the reflectivity becomes a negative number dependent on the material of the scatterer.
\end{itemize}


\subsubsection{Scattered Field}
By subtracting the incident and total field equations we can find that $\usc$ satisfies
\begin{equation}
\left(\laplacian-c_0^{-2}\partial_t^2\right)\usc(x,t)=-v(x)\partial_t^2 u(x,t)
\end{equation}
We can then solve for the scattered field as
\begin{equation}
\usc(x,t) = \int \int  g_0(x-z,t-\tau)\vc(z)\partial_{\tau}^2 u(z,\tau) \dd z \dd \tau
\end{equation}
We can use the solution for the scattered field to find a nonlinear integral equation for the total field.
\begin{equation}
u(x,t) =\uin(x,t) +  \int \int  g_0(x-z,t-\tau)\vc(z)\partial_{\tau}^2 u(z,\tau) \dd z \dd \tau
\end{equation}


\subsubsection{Neumann Series}
When the incident field interacts with a target it gets scattered.  This radiation can again get scattered an arbitrary number of times.
The field that has been scattered $k$ times is given by
\begin{equation}
\begin{tabular}{ll}
 u_k=&\int \int g_0(x-z_1,t-\tau_1) v(z_1) \partial_{\tau_1}^2 ...\\
  &\int \int  g_0(z_{k-1}-z_k,\tau_{k-1}-\tau_k) v(z_k) \partial_{\tau_k}^2 \\
  &\uin(z_k,t-\tau_k)\dd z_k \dd \tau_k..\dd z_1 \dd \tau_1.\\
\end{tabular}
\end{equation}\pause
If the field has yet to be scattered, then $k=0$ and
\begin{equation}u_0(x,t)=\uin(x,t)\end{equation}\pause
The total field is then simply the sum of all $u_k$
\begin{equation}
u(x,t)=\lim_{n\rightarrow \infty } \sum^{n}_{k=0}{ u_k }
\end{equation}
This is the solution we find by iterating the recurrence equation for the total field.



\subsubsection{Born approximation}
Convergence of this series is dependent on $v$ as well as the incident field. Conditions for rapid convergence include
\begin{itemize}
\item Low reflectivity targets.
\item Well spaced scatterers.
\item Power loss each time the field scatters
\end{itemize}
We take the first two terms in the Neumann series as an approximation to the total field.  This means that only the radiation from the first interaction is non negligible.
\begin{equation}
u(x,t) \approx \uin(x,t) + \int \int g_0(x-z,t-\tau)v(z)\partial_{\tau}^2 \uin(z,\tau)\dd z \dd \tau.
\end{equation}
With this truncation of the series we have that the scattered field is approximated by
\begin{equation}
\usc(x,t) \approx \int \int g_0(x-z,t-\tau)v(z)\partial_{\tau}^2 \uin(z,\tau)\dd z \dd \tau.
\end{equation}



\subsubsection{Convolution}
The name of the integral we have been using is called a convolution integral.  We can write this integral succinctly by use of the convolution operator. The convolution operator  in space and time $\convxt$ of $f:\R^d \times \R \to
\C$ and $g:\R^d\times \R \to \C$ is given by
\begin{equation}\label{eq: convxtdef}
f(x,t)\convxt g(x,t) = \int_{\R^d} \int_{\R} f(x-z,t-\tau)g(z,\tau) d \tau \dd z
\end{equation}
Likewise the convolution only in space $\convx$ is given by
\begin{equation} \label{eq: convxdef}
f(x,t)\convx g(x,t) = \int_{\R^d} f(x-z,t)g(z,t)\dd z .
\end{equation}
Using this notation we have that the scattered field satisfies
\begin{equation}
\usc(x,t) \approx g_0(x,t)\convxt v(x)\partial_{t}^2 \uin(x,t).
\end{equation}



\subsubsection{Temporal Fourier Transform}
If $u: \R^3 \times \R \to \C$ as $(x,t) \mapsto u(x,t)$ we define the \textbf{temporal Fourier transform} $U(x,f)$ transforming $t \to f$ as
\begin{equation}
U(x,f)=\int u(x,t)e^{-2\pi i tf}\dd t.
\end{equation}
Capital letters will denote Temporal Fourier transforms, i.e.  $F$ will denote the  Temporal Fourier transform of the
function $f$.
Properties of the Temporal Fourier Transform include:
\begin{itemize}
\item Transforms time variable into frequency variable.
\item Turns time derivatives into polynomials of new variable.
\item Has an inverse given by \\ \begin{equation}u(x,t)=\int U(x,f)e^{2\pi i tf}\dd f.\end{equation}
\end{itemize}

Restrictions of the Temporal Fourier Transform:
\begin{itemize}
\item Function must be compactly supported.  This essentially means that a function must die off at its boundaries.
\end{itemize}

\subsubsection{Convolution Theorem}
The Convolution Theorem relating convolution to multiplication when applying the Temporal Fourier Transform is given by
\begin{equation} \label{eq: convthm}
\int_{\R}f(x,t)\convxt g(x,t)e^{-2\pi i tf}\dd t=F(x,f)\convx G(x,f)
\end{equation}

Going into the frequency domain $g_0(x,t)$, $\uin(x,t)$, $\usc(x,t)$, and $j(x,t)$ become $G_0(x,f)$, $\Uin(x,t)$, $\Usc(x,t)$, and $J(x,t)$.  The transformed incident field satisfies
\begin{equation} \label{eq: Uin}
\left(\laplacian+c_0^{-2}\omega^2\right)\Uin(x,f)=-J(x,f).
\end{equation}
By the convolution theorem the transformed scattered field satisfies
\begin{equation} \label{eq: Usc}
\Usc(x,f) \approx - G_0(x,f)\convx \vc(x) \omega^2 \Uin(x,f)
\end{equation}
where $\omega=2\pi f$ and
\begin{equation}\label{eq: greensfreq}
G_0(x,f)=\frac{e^{-2\pi if|x|/c_0}}{4\pi|x|}.
\end{equation}


\subsection{Multiple Input Multiple Output RADAR}





Each transmitter in a MIMO RADAR system emits its own signal into space which mixes with the signals from the other transmitters.
\begin{figure}[h]\begin{center}\includegraphics[scale=.6]{mimoradartransmit}\end{center}\end{figure}


\subsubsection{Isotropic Point Like Transmitters}
The Dirac delta function is the function that satisfies
\begin{equation} \label{equ: deltasupport}
\delta(x)=\left\{0,  x \neq 0\right\}
\end{equation}
\begin{equation} \label{equ: deltaintegrate}
\int{\delta(z)} dz =1.
\end{equation}
\begin{itemize}
\item Zero everywhere except for when its argument is zero.
\item Has equal directional gain.
\item For a smooth function $f:\R^d  \to \C$ \\ \begin{equation}
\int {f(z) \delta(z-x) }\dd z=f(x).
\end{equation}
\end{itemize}
We define a point isotropic antenna located at $z$ as an antenna whose radiation pattern is given by
\begin{equation}
J(x,f)=P_\ell(f)\delta(x-z)
\end{equation}


\subsubsection{Incident Field}
We work with MIMO RADAR systems that consist of $N_T$  point like isotropic transmitters located at positions $x_\ell$ and transmitting signals $P_\ell(f)$ so that
$J(x,f)$ is just the sum of the transmitter signals at their respective locations
\begin{equation} \label{eq: source}
J(x,f)=\sum_{\ell=1}^{N_T} P_\ell(f)\delta(x-x_\ell).
\end{equation}

The incident field in the frequency domain is then
\begin{equation} \label{eq: Uinsolve}
\Uin(x,f)=\sum_{\ell=1}^{N_T}P_\ell(f)\frac{e^{-i\omega|x-x_\ell|/c_0}}{4\pi|x-x_\ell|}.
\end{equation}






\begin{figure}[h]
\begin{center}
  \subfloat{\includegraphics[width=0.35\textwidth]{3above3below}}
  \subfloat{\includegraphics[width=0.35\textwidth]{wedge}}
  \caption{Example: Incident field for A MIMO Configuration}
  \\
  \subfloat{\includegraphics[width=0.28\textwidth]{firstfloor}}
  \subfloat{\includegraphics[width=0.28\textwidth]{secondfloor}}
  \subfloat{\includegraphics[width=0.28\textwidth]{thirdfloor}}
\end{center}
\end{figure}




\section{MIMO RADAR Receiving}
The transmitted field from a MIMO RADAR system interacts with targets and gets scattered.  Each receiver measures the scattered field in some region in space.
\begin{center}\includegraphics[scale=.6]{mimoradarscatter}\end{center}



\begin{figure}[h]
\begin{center}
  \includegraphics[scale=.2 ]{scattered}
\end{center}
\caption{Example: Scattered Field for Point Scatterers}
\end{figure}





\subsection{Inverse Problem}


\subsubsection{MIMO Data Model}
The data we are given is governed by the scattered field.
\begin{itemize}
\item We have $N_R$ receivers that measure the scattered field at the points $y_j$.
\item The scattered field  at the $j$th receiver is approximately \\ \begin{equation}
\Usc(y_j,f) \approx - \sum_{\ell=1}^{N_T}\omega^2 P_\ell(f)\int_{\R^3}\frac{e^{-i\omega(|y_j-z|+|z-x_\ell|)/c_0}}{(4\pi)^2|y_j-z||z-x_\ell|}\vc(z)\dd z.
\end{equation}
\item Data is collected from all receivers at the same time.
\end{itemize}





%\subsection{Linear System Method}
\subsubsection{Linear System}
One way to recover an image is to approximate the integral of the reflectivity function as a summation over sampled points of $v$.  This means that
\begin{equation}
\Usc(y_j,f) \approx - \sum_{\ell=1}^{N_T}\omega^2 P_\ell(f)\sum_{k=1}^K\frac{e^{-i\omega(|y_j-z_k|+|z_k-x_\ell|)/c_0}}{(4\pi)^2|y_j-z_k||z_k-x_\ell|}\vc(z_k).
\end{equation}
if we let $\vec{v}_k=v(z_k)$ and define the matrix $M(f)$ as
\begin{equation}
M(f)_{jk}=- \sum_{\ell=1}^{N_T}\omega^2 P_\ell(f)\frac{e^{-i\omega(|y_j-z_k|+|z_k-x_\ell|)/c_0}}{(4\pi)^2|y_j-z_k||z_k-x_\ell|}
\end{equation}
Then we have that
\begin{equation}
M(f)\vec{v}=R(f)
\end{equation}




\subsubsection{Sampled Data}
The first step away from theory and towards processing is establishing a useful sampling scheme.   We collect samples of $R(w)$ and $P(w)$ over the set of sampled frequencies $\{f_1,\dotsc,f_{N_S}\}$.
   We sample the receive and transmit vectors as
   \begin{equation} \label{eq: srv }
   {\cal R}=
		\begin{pmatrix}
			R(f_1)\\
			\vdots\\
			R(f_{N_S})
		\end{pmatrix}.
	 \end{equation}
	\begin{equation} \label{eq:stv}
	{\cal P}=
			\begin{pmatrix}
			P(f_1)\\
			\vdots\\
			P(f_{N_S})
		\end{pmatrix}.
	\end{equation}



\subsubsection{Sampling the Matrix}
When we sample the matrix $M(f)$ as $M$
\begin{equation}
M=\begin{pmatrix}
			M(f_1)\\
			\vdots\\
			M(f_{N_S})
		\end{pmatrix}.
\end{equation}
the matrix equation becomes
\begin{equation}
M\vec{v}={\cal R}
\end{equation}
An image can then be recovered by:
\begin{enumerate}
\item Sample received data at different frequencies.
\item Generate the sampled matrix $M$ according to those frequencies.
\item Solve the corresponding linear system for $\vec{v}$.
\item Map $\vec{v}$ back into a grid for our image.
\end{enumerate}



%\subsection{Fourier Based Methods}
\subsubsection{Channel Matrix Model}

\begin{itemize}
\item Transmit Vector \\
Contains information about all the transmitted signals in one vector $P$.\pause
\item Receive Vector\\
Contains all the received data in one vector $R$.\pause
\item Channel Matrix $H$\\
Describes the transformation between transmitted and the received signals.
\end{itemize}
\begin{figure}[h]\begin{center}\includegraphics[scale=.45]{channelmatrix}\end{center}\end{figure}


\subsubsection{Channel Matrix Linear System}
The channel matrix $H(f) \in \C^{N_R \times N_T}$ is defined as
\begin{equation} \label{eq: Hjldef}
H_{j,\ell}(f)= - \int_{\R^3}\omega^2\frac{e^{-i\omega(|y_j-z|+|z-x_\ell|)/c_0}}{(4\pi)^2|y_j-z||z-x_\ell|}\vc(z)\dd
z.
\end{equation}
Then
\begin{equation}
\vec{R}_j(f) \approx \sum_{\ell=1}^{N_T}H_{j,\ell}(f)\vec{P}_\ell(f)
\end{equation}
so that
\begin{equation}
R(f) \approx H(f)P(f).
\end{equation}



\subsubsection{The Far Field Approximation}
Suppose we are given two points $x,y \in \R^d$.  We wish to approximate $|x-y|$
and $\frac{1}{|x-y|}$.  To do this we first note that
\begin{equation}\label{eq: ffsqrt}
|x-y|=\sqrt{(x-y)\cdot(x-y)}=|x|\sqrt{1-2 \frac{\widehat{x}\cdot y}{|x|} + \frac{|y|}{|x|}^2}
\end{equation}
If $|x| \gg |y|$ then we can use the binomial series to find that
\begin{equation}\label{eq: ffappnum}
|x-y|\approx|x|-\widehat{x}\cdot y
\end{equation}
and
\begin{equation}\label{eq: ffappden}
\frac{1}{|x-y|} = \frac{1}{|x|\sqrt{1-2 \frac{\widehat{x}\cdot y}{|x|} + \frac{|y|}{|x|}^2}} \approx \frac{1}{|x|}+\frac{\widehat{x}\cdot y}{|x|^2}\approx \frac{1}{|x|}
\end{equation}



\subsubsection{Spatial Fourier Transform}
For $f:\R^d \times \R \to \C$, that is f maps a $d$-dimensional real vector and a real scalar to a complex
number, let $F$ denote the \textbf{Fourier transform}  of $f$
defined as
\begin{equation} \label{eq: FT}
F(\xi,t)=\int_{\R^d} f(x,t)e^{-2\pi ix \cdot \xi}\dd x.
\end{equation}
Capital letters will denote Spatial Fourier transforms as they do Temporal Fourier transforms when no ambiguity is present.  Like the temporal Fourier transform the spatial Fourier transform is also invertible.  The inverse transform is given by
\begin{equation}
f(x,t)=\int_{\R^d}F(\xi,t) e^{2\pi ix \cdot \xi}\dd \xi.
\end{equation}







\subsubsection{Making Use Of The Channel Matrix}
We keep our antennas far from targets so $|x_\ell| \gg |z|, |y_j| \gg |z|$ for all $z \in \supp(V)$. Then
\begin{equation}
|x_\ell - z | \approx |x_\ell|-\widehat{x_\ell}\cdot z
\end{equation}
\begin{equation}
|y_j-z|\approx |y_j|-\widehat{y_j}\cdot z
\end{equation}
\begin{equation}
\frac{1}{|x_\ell - z |} \approx \frac{1}{|x_\ell|}
\end{equation}
\begin{equation}
\frac{1}{|y_j - z|} \approx \frac{1}{|y_j|}.
\end{equation}
The channel matrix is then related to the scene by
\begin{equation}
H_{j,\ell}(f) \approx -\frac{\omega^2e^{-i\omega(|y_j|+|x_\ell|)}}{(4\pi)^2|y_j||x_\ell|}\overline{\Vc}\left(\frac{f}{c_0}\left(\widehat{y_j}+\widehat{x_\ell}\right)\right)
\end{equation}








\subsubsection{Sampling The Channel Matrix}
Our purpose is to form an image approximating $v$. If we know $H(f)$ for all $f$, then we approximately know $\Vc$ along the $N_TN_R$ lines $\frac{f}{c_0}\left(\widehat{y_j}+\widehat{x_\ell}\right)$ in the Fourier domain by the relation
\begin{equation}
\Vc\left(\frac{f}{c_0}\left(\widehat{y_j}+\widehat{x_\ell}\right)\right)\approx \overline{H}_{j,\ell}(f)  \frac{(4\pi)^2|y_j||x_\ell|e^{i\omega(|y_j|+|x_\ell|)}}{\omega^2}
\end{equation}
 when $\omega \neq 0$.
 We can only estimate a sampled form of our channel matrix.
    We sample the channel matrix as
    \begin{equation} \label{eq: Hsample}
	{\cal H}=
			\begin{pmatrix}
			&H(f_1) & \cdots   &(0) \\
			&\vdots &\ddots    &     \\
			&(0)    &          &H(f_{N_S})
		\end{pmatrix}.
    \end{equation}
    giving us
    \begin{equation}\label{eq: sampledequation}
    {\cal R}= {\cal H}{\cal P}
    \end{equation}







\subsubsection{Pulsed MIMO RADAR}
There is no way sampling can be used to get sufficient information to recover the channel matrix without some extra dimension of information.  Increasing the number of frequency samples, transmitters or receivers will increase the number of variables to estimate in the channel matrix. We need more information.
\begin{itemize}
\item Have transmitters send their signals.\pause
\item Radiation propagates through space interacts with scatterers and is received by receivers.\pause
\item Radiation that is not picked up by receivers eventually disperses.\pause
\item Transmitters wait until radiation is gone and send new set of signals.
\end{itemize}




\subsubsection{Pulsed MIMO RADAR Model}
Treat each pulse independently.
Denote the sampled receive and transmit vectors from the $p$th pulse ${\cal R}_p$ and ${\cal P}_p$ respectively. Then
\begin{equation}\label{eq: pulse}
{\cal R}_p={\cal H}{\cal P}_p.
\end{equation}
We group the equations for the all $N_P$ pulses together into a single matrix equation
\begin{equation}
\begin{pmatrix}
{\cal R}_1,\ldots {\cal R}_{N_P}
\end{pmatrix}
={\cal H}
\begin{pmatrix}
{\cal P}_1,\ldots {\cal P}_{N_P}
\end{pmatrix}.
\end{equation}
The more pulses we take, the better we can estimate the channel matrix.








%\subsubsection{Fourier Inverse Approximation Method}

\subsubsection{Fourier Inverse Approximation}
At this point one of the available options for recovering our scene is to use the recovered channel matrix to try approximate the integral that defines the spatial inverse Fourier transform of $V$. We can recover our scene by the following process:\pause
\begin{enumerate}
\item  Estimate channel matrix.\pause
\item  Use the estimate to find the Fourier transform of the scene at several points.\pause
\item  Use the realness property of the reflectivity function to get more data.\pause
\item  Apply the rectangle rule to approximate inverse Fourier transform of $V$ to recover an estimate of $v$.
\end{enumerate}


\subsubsection{Discrete Fourier Transform}
For a discrete function of $d$ variables $f:N_1 \times ... \times N_d \to \C$
where $N_i=\{0,...M_i-1\}$ define the \textbf{Discrete Fourier transform} of $f$ denoted $F(k_1,...,k_d)$ as
\begin{equation}\label{eq:DFT}
F(k_1,...,k_d)=\sum_{n_1=0}^{M_1-1}...\sum_{n_d=0}^{M_d-1} f(n_1,...,n_d) e^{-2 \pi i (\frac{k_1 n_1}{M_1}+...+\frac{k_d n_d}{M_d})}.
\end{equation}
The \textbf{Inverse Discrete Fourier Transform} of $F:K_1
\times...\times K_d$ is given by
\begin{equation}\label{eq:IDFT}
f(n_1,...,n_d)=\frac{1}{M_1...M_d}\sum_{k_1=0}^{M_1-1}...\sum_{k_d=0}^{M_d-1} F(k_1,...,k_d) e^{2 \pi i (\frac{k_1 n_1}{M_1}+...+\frac{k_d n_d}{M_d})}.
\end{equation}



\subsubsection{Discrete Fourier Transform Method Restrictions}
We restrict our scene to consist of a finite number of point like scatterers that lie on a grid of
$M_1 \times M_2$  points on the $xy$ plane.  The $x_1$ and $x_2$ spacing of the
points are given by $\Delta x_1$ and $\Delta x_2$ respectively. The
$(n_1,n_2)$th position on the  grid can be found explicitly as
\begin{equation}
({x_1}_{n_1},{x_2}_{n_2})=(n_1\Delta x_1,n_2\Delta x_2).
\end{equation}

The 2-d Fourier transform of $\vc$ then becomes
\begin{equation}
\Vc(\xi_1,\xi_2)=\sum_{n_1=0}^{M_1-1} \sum_{n_2=0}^{M_2-1}
v({x_1}_{n_1},{x_2}_{n_2})e^{-2\pi i(n_1\Delta x_1 {\xi_1}+n_2\Delta x_2{\xi_2})}.
\end{equation}




\subsubsection{Recovering Our Scene}
Evaluating $V$ at the discrete points $(\frac{m_1}{M_1 \Delta x_1},\frac{m_2}{M_2\Delta x_2})$ we have
\begin{equation}\label{eq: DiscreteV}
\Vc(\frac{m_1}{M_1 \Delta x_1},\frac{m_2}{M_2\Delta x_2})=\sum_{n_1=0}^{M_1-1} \sum_{n_2=0}^{M_2-1}
v({x_1}_{n_1},{x_2}_{n_2})e^{-2\pi i( \frac{{n_1}{m_1}}{M_1}+\frac{{n_2}{m_2}}{M_2})}.
\end{equation}

This is just the discrete Fourier transform of $v({x_1}_{n_1},{x_2}_{n_2})$ allowing us to use
the inverse discrete Fourier transform to find that
\begin{equation}\label{eq: recovereddiscretev}
v({x_1}_{n_1},{x_2}_{n_2})=\frac{1}{M_1 M_2}\sum_{m_1=0}^{M_1-1} \sum_{m_2=0}^{M_2-1}
\Vc(\frac{m_1}{M_1 \Delta x_1},\frac{m_2}{M_2\Delta x_2})e^{2\pi i( \frac{{n_1}{m_1}}{M_1}+\frac{{n_2}{m_2}}{M_2})}.
\end{equation}



\subsubsection{Image Formation algorithm}
We can then recover our scene by the following process:\pause
\begin{enumerate}
\item  Estimate channel matrix.\pause
\item  Use the estimate to find the Fourier transform of the scene at several points.\pause
\item  Use realness property of reflectivity function to get more fourier data.\pause
\item  Interpolate $V$ onto a grid.
\item  Perform inverse discrete Fourier transform on interpolated data.
\end{enumerate}

