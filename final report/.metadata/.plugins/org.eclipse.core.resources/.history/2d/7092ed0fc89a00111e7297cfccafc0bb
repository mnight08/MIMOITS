\documentclass[xcolor=svgnames,compressed,11pt,a4paper,oneside]{report}
\usecolortheme[named=MidnightBlue]{structure}
\usepackage{beamerthemeDresden}
%% Language %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[USenglish]{babel} %francais, polish, spanish, ...
\usepackage[T1]{fontenc}
\usepackage[ansinew]{inputenc}
\usepackage{lmodern} %Type1-font for non-english texts and characters
%% Packages for Graphics & Figures %%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx} %%For loading graphic files
\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{setspace}
\usepackage[]{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{mathrsfs}
\usepackage{subfloat}
\usepackage{subfigure}

\newcommand{\dd}{\; \mathrm{d}}
\newcommand{\lti}{\lim_{n\to \infty}}
\newcommand{\stio}{\sum_{n=0}^{\infty}}
\newcommand{\stii}{\sum_{n=1}^{\infty}}
\newcommand{\utii}{\bigcup_{n=1}^{\infty}}
\newcommand{\MM}{\mathcal{M}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\comp}{\mathscr{C}}
\newcommand{\EE}{\mathcal{E}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\Rn}{\mathbf{R}^n}
\newcommand{\Rm}{\mathbf{R}^m}
\newcommand{\MF}{\mathcal{M_F}}
\newcommand{\bsol}{\begin{proof}[Solution]}
\newcommand{\esol}{\end{proof}}
\newcommand{\laplacian}{\nabla^2}
\newcommand{\varep}{\varepsilon}
\newcommand{\interior}{\mathrm{Int}}
\newcommand{\uin}{u_{\mathrm{in}}}
\newcommand{\usc}{u_{\mathrm{sc}}}
\newcommand{\Uin}{U_{\mathrm{in}}}
\newcommand{\Usc}{U_{\mathrm{sc}}}
\newcommand{\convx}{{{*}_{_{ x}}}}
\newcommand{\convxt}{{{ *}_{_{x,t}}}}
\newcommand{\vc}{v}
\newcommand{\Vc}{V}
%\newcommand{\frame}{\begin{frame}}
%\newcommand{\endframe}{\end{frame}}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\supp}{\mathrm{supp}}
\begin{document}

\title{In-Field Local Image Reconstruction}
\author{Alex Martinez}
\date{\today}
\frame{\titlepage}

\input{background}
\input{forwardmodel}
\input{imageformation}


\appendix
\end{document}

