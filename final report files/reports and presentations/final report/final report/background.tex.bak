\section{Background}

\begin{frame}{Overview of RADAR}
\begin{itemize}
 The general problem of radar imaging is to use some physical model
 for a transmitted signal, and measurements of the signal that is scattered back to
 a receiver by a scene to attempt to derive information about the scene.
\item How do we see when it is dark? \pause \\Use light.\pause
\item What is light? \pause \\Electromagnetic Radiation.\pause
\item How do we see with light?  \pause \\It is scattered by objects and measured by eyes.\pause
\item What happens if light does not work? \pause \\Use other types of Electromagnetic Radiation.\pause
\item How can we use other types of Electromagnetic Radiation? \pause \\Let it bounce off of objects and measure it.
\end{itemize}
\end{frame}


\begin{frame}{Simple RADAR Transmit}
In a classical RADAR system a single transmitter emits its signal into space.
\includegraphics[scale=.7]{simpleradartransmit}
\end{frame}


\begin{frame}{Simple RADAR Receiving}
The field from the transmitter interacts with targets and gets scattered and measured by the receiver.
\includegraphics[scale=.7]{simpleradarscatter}
\end{frame}

\begin{frame}{Examples of RADAR Systems}
RADAR systems can be broken up into two major categories
\begin{itemize}
\item Passive Imaging
\begin{itemize}
\item No control over target illumination.
\item Difficult to detect if an enemy is imaging.
\item Hitch Hiker Imaging.
\item Human Vision.
\end{itemize}
\item Active RADAR
\begin{itemize}
\item User maintains control of input radiation.
\item More powerful than Passive Imaging.
\item Easier to detect than Passive Imaging.
\item Bistatic SAR.
\item Most rudimentary RADAR systems.
\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}{The Project}
The original plan was to create a RADAR system that would allow a set of users to recover information about their local surroundings by using a distributed MIMO configuration.  Some of the problems that needed to be addressed are:
\begin{itemize}
\item Distributing the work load for image formation
\item Establishing a useful image formation algorithm
\item Working out a communication scheme for the users
\end{itemize}

The problems with this plan are that
\begin{itemize}
\item The plan is overzealous.
\item It has several difficult sub problems.
\item Not feasible in a single semester.
\end{itemize}
Our focus changed to $2$-d MIMO RADAR imaging for stationary targets.
\end{frame}

\begin{frame}{Applications and Advantages}
MIMO RADAR has application to several areas including

\begin{itemize}
\item  multi-user RADAR systems.
\item  Military applications.

\item  

\end{itemize}
Advantages over conventional RADAR include
\begin{itemize}
\item  Higher resolution for detecting ground moving targets.
\item  Removing false targets.
\end{itemize}

\end{frame}


\begin{frame}{Outline}
The general structure of developing imaging techniques consist of the following:
\begin{itemize}
\item  Using a physical model to relate data to target scene.  This is called the forward model
\item  Solving for the scene in terms of the given data.  Called the inverse problem.
\item Testing the solution to the inverse problem

\end{itemize}
\end{frame}








