\section{Project Results}



\subsection{Linear System Reconstruction}
The idea for this reconstruction method is to look at how our targets are transformed into our received data.  This becomes a linear algebra problem.  The details of exactly what this transformation looks like are included in the low level model and reconstruction section.

\section{Simulation}
We generated randomly sampled signals and attempted to recover a single target located at the origin.  This was performed six times with the same parameters save for the signals.  The recovered images are below:
\begin{figure}
  \subfloat{\includegraphics[width=0.28\textwidth]{Linearsystemsinglescatterer1}}
  \subfloat{\includegraphics[width=0.28\textwidth]{Linearsystemsinglescatterer2}}
  \subfloat{\includegraphics[width=0.28\textwidth]{Linearsystemsinglescatterer3}}
  \\
  \subfloat{\includegraphics[width=0.28\textwidth]{Linearsystemsinglescatterer4}}
  \subfloat{\includegraphics[width=0.28\textwidth]{Linearsystemsinglescatterer5}}
  \subfloat{\includegraphics[width=0.28\textwidth]{Linearsystemsinglescatterer6}}
\end{figure}


\section{Criticism of Imaging Scheme}
This imaging scheme has several down sides.  The most prevalent are:
\begin{itemize}
\item $M$ tends to be rank deficient if the transmitter and receiver locations are not sporadic enough.
\item The size of $M$ can be very large.  If we are looking for a $1,000 \times 1,000$ resolution image using a single receiver then we must generate at least $1,000,000$ entries.
\end{itemize}
On the positive side we have:
\begin{itemize}
\item Easily adaptable to new processing techniques such as compressive sensing. $\vec{v}$ is a good candidate for a sparse vector
\item No extra information is needed to recover an Image.
\end{itemize}





\subsection{Fourier Based Reconstruction}
The idea behind the next two reconstruction methods is to look at how our targets transform our input signals into our received data.  We see that if we know what this transformation looks like then we can find samples of an invertible function of our targets.  This means that we can try to
\subsubsection{Rectangle Rule Reconstruction}
The inverse to the function of our targets is given by an integral equation over the functions values. Here we approximate the distribution of our targets in space by using a sum to approximate the integral with the samples of the function values given by the transformation.  A more detailed description is given in the low level model and reconstruction section.


\section{Simulation}
The recovered images below were recovered using low bandwidth and low frequency samples.
\begin{figure}
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral1}}
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral2}}
  \\
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral3}}
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral4}}
\end{figure}


\section{More Simulation}
These recovered images below used high bandwidth and high frequency samples.
\begin{figure}
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral5}}
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral6}}
  \\
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral7}}
  \subfloat{\includegraphics[width=0.35\textwidth]{approximateintegral8}}
\end{figure}

\section{Criticism of Imaging Scheme}
This imaging scheme has several down sides.  The ones that stand out the most are
\begin{itemize}
\item Images recovered are not very accurate.
\item Runs slow.
\item Requires an extra dimension of information to even have a chance at building an image.

\end{itemize}
On the positive side we have
\begin{itemize}
\item Does not require a large matrix in memory to work with
\item Algorithm is iterative meaning that new information can be easily incorporated
\end{itemize}



\subsubsection{Discrete Fourier Transform}
In this method we go about estimating the inverse of the function in a different way.  This is based off of a discrete transformation that approximates the transformation


The important distinction between this method and the previous is that algorithms exist to perform the discrete Fourier transform much faster than we can use the rectangle rule to approximate an integral.

\section{1-d Simulation 2 scatterers}
\begin{center}
\includegraphics[scale=.55]{2scatters10frequencysamples}
\end{center}


\section{1-d Simulation 2 scatterers}
\begin{center}
\includegraphics[scale=.55]{2scatters100frequencysamples}
\end{center}


\section{1-d Simulation 2 scatterers}
\begin{center}
\includegraphics[scale=.55]{2scatters1000frequencysamples}
\end{center}


\section{1-d Simulation 2 scatterers}
\begin{center}
\includegraphics[scale=.55]{2scatters10000frequencysamples}
\end{center}


\section{1-d Simulation 2 scatterers}
\begin{center}
\includegraphics[scale=.55]{2scatters100000frequencysamples}
\end{center}


\section{1-d Simulation 2 scatterers}
\begin{center}
\includegraphics[scale=.55]{2scatters1000000frequencysamples}
\end{center}


\section{1-d Simulation 10 scatterers}
\begin{center}
\includegraphics[scale=.55]{10scatters10frequencysamples}
\end{center}

\section{1-d Simulation 10 scatterers}
\begin{center}
\includegraphics[scale=.55]{10scatters100frequencysamples}
\end{center}

\section{1-d Simulation 10 scatterers}
\begin{center}
\includegraphics[scale=.55]{10scatters1000frequencysamples}
\end{center}

\section{1-d Simulation 10 scatterers}
\begin{center}
\includegraphics[scale=.55]{10scatters10000frequencysamples}
\end{center}

\section{1-d Simulation 10 scatterers}
\begin{center}
\includegraphics[scale=.55]{10scatters100000frequencysamples}
\end{center}

\section{1-d Simulation 10 scatterers}
\begin{center}
\includegraphics[scale=.55]{10scatters1000000frequencysamples}
\end{center}




\subsubsection{Criticism of Imaging Scheme}
The major down sides of this imaging scheme are that
\begin{itemize}
\item Image may be inaccurate due to interpolation error.
\item Implementation is not as simple as approximating the integral.
\item Transmitters and receivers must be far from the targets.
\item Needs several pulses to reasonably recover channel matrix.
\end{itemize}
On the positive side we have
\begin{itemize}
\item Can be done significantly faster than approximating the integral.
\item Does not need to work with all data at one time.
\end{itemize}














