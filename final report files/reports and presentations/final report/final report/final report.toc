\select@language {USenglish}
\contentsline {section}{\numberline {1}Document Structure}{2}
\contentsline {section}{\numberline {2}High Level Overview}{3}
\contentsline {subsection}{\numberline {2.1}Overview of RADAR}{3}
\contentsline {subsubsection}{\numberline {2.1.1}Conventional RADAR System}{3}
\contentsline {subsubsection}{\numberline {2.1.2}Examples of RADAR Systems}{4}
\contentsline {subsection}{\numberline {2.2}The Project}{5}
\contentsline {subsection}{\numberline {2.3}MIMO RADAR Overview}{5}
\contentsline {subsubsection}{\numberline {2.3.1}Multiple Input Multiple Output - (MIMO) Communication}{5}
\contentsline {subsubsection}{\numberline {2.3.2}MIMO RADAR}{6}
\contentsline {subsubsection}{\numberline {2.3.3}Applications and Advantages}{6}
\contentsline {subsection}{\numberline {2.4}Project Outline}{7}
\contentsline {section}{\numberline {3}Low Level Model and Reconstruction}{7}
\contentsline {subsection}{\numberline {3.1}Forward Model}{7}
\contentsline {subsubsection}{\numberline {3.1.1}The Wave Equation}{7}
\contentsline {subsubsection}{\numberline {3.1.2}Scalar Wave Model}{8}
\contentsline {subsubsection}{\numberline {3.1.3}Reflectivity function}{8}
\contentsline {subsubsection}{\numberline {3.1.4}Scattered Field}{8}
\contentsline {subsubsection}{\numberline {3.1.5}Neumann Series}{9}
\contentsline {subsubsection}{\numberline {3.1.6}Born approximation}{9}
\contentsline {subsubsection}{\numberline {3.1.7}Convolution}{10}
\contentsline {subsubsection}{\numberline {3.1.8}Temporal Fourier Transform}{10}
\contentsline {subsubsection}{\numberline {3.1.9}Convolution Theorem}{11}
\contentsline {subsection}{\numberline {3.2}Multiple Input Multiple Output RADAR}{11}
\contentsline {subsubsection}{\numberline {3.2.1}Isotropic Point Like Transmitters}{11}
\contentsline {subsubsection}{\numberline {3.2.2}Incident Field}{12}
\contentsline {section}{\numberline {4}MIMO RADAR Receiving}{13}
\contentsline {subsection}{\numberline {4.1}Inverse Problem}{13}
\contentsline {subsubsection}{\numberline {4.1.1}MIMO Data Model}{13}
\contentsline {subsubsection}{\numberline {4.1.2}Linear System}{14}
\contentsline {subsubsection}{\numberline {4.1.3}Sampled Data}{14}
\contentsline {subsubsection}{\numberline {4.1.4}Sampling the Matrix}{14}
\contentsline {subsubsection}{\numberline {4.1.5}Channel Matrix Model}{15}
\contentsline {subsubsection}{\numberline {4.1.6}Channel Matrix Linear System}{15}
\contentsline {subsubsection}{\numberline {4.1.7}The Far Field Approximation}{16}
\contentsline {subsubsection}{\numberline {4.1.8}Spatial Fourier Transform}{16}
\contentsline {subsubsection}{\numberline {4.1.9}Making Use Of The Channel Matrix}{16}
\contentsline {subsubsection}{\numberline {4.1.10}Sampling The Channel Matrix}{17}
\contentsline {subsubsection}{\numberline {4.1.11}Pulsed MIMO RADAR}{17}
\contentsline {subsubsection}{\numberline {4.1.12}Pulsed MIMO RADAR Model}{17}
\contentsline {subsubsection}{\numberline {4.1.13}Fourier Inverse Approximation}{18}
\contentsline {subsubsection}{\numberline {4.1.14}Discrete Fourier Transform}{18}
\contentsline {subsubsection}{\numberline {4.1.15}Discrete Fourier Transform Method Restrictions}{18}
\contentsline {subsubsection}{\numberline {4.1.16}Recovering Our Scene}{19}
\contentsline {subsubsection}{\numberline {4.1.17}Image Formation algorithm}{19}
\contentsline {section}{\numberline {5}Project Results}{19}
\contentsline {subsection}{\numberline {5.1}Goals}{19}
\contentsline {subsection}{\numberline {5.2}Notes about Simulation}{20}
\contentsline {subsection}{\numberline {5.3}Simulation of Linear System Reconstruction Method}{21}
\contentsline {subsubsection}{\numberline {5.3.1}Simulation}{21}
\contentsline {subsubsection}{\numberline {5.3.2}Criticism of Imaging Scheme}{21}
\contentsline {subsection}{\numberline {5.4}Simulation of Fourier Based Reconstruction Methods}{22}
\contentsline {subsubsection}{\numberline {5.4.1}Simulation of Rectangle Rule Reconstruction Method}{22}
\contentsline {subsubsection}{\numberline {5.4.2}Simulation}{22}
\contentsline {subsubsection}{\numberline {5.4.3}Criticism of Imaging Scheme}{23}
\contentsline {subsection}{\numberline {5.5}Simulation of Discrete Fourier Transform Method}{23}
\contentsline {subsubsection}{\numberline {5.5.1}1-d Simulation 2 scatterers}{24}
\contentsline {subsubsection}{\numberline {5.5.2}1-d Simulation 10 scatterers}{24}
\contentsline {subsubsection}{\numberline {5.5.3}Criticism of Imaging Scheme}{24}
\contentsline {section}{\numberline {6}Simulation Documentation}{25}
