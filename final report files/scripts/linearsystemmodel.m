%define constants and stuff
c_0=3*10^8;
N_T=5;
N_R=5;
K=3;
N_S=100;
B=100;
f0=550;

w=2*pi*(f0-B/2):2*pi*B/N_S:2*pi*(f0+B/2);
%left corner of image
imageposition=[-50,-50]';
%height, width
imagedimensions=[100,100]';
M1=10;
M2=10;
deltax1=imagedimensions(1)/M1;
deltax2=imagedimensions(2)/M2;
x1=imageposition(1)+deltax1/2:deltax1:imageposition(1)+M1*deltax1-deltax1/2;
x2=imageposition(2)+deltax2/2:deltax2:imageposition(2)+M2*deltax2-deltax2/2;

Signals=struct('signal',@signal);
P=zeros(N_T,N_S);
Y=zeros(2,N_R);
X=zeros(2,N_T);
s=1;
l=1;
j=1;
%transmitters
X(:,1)=[100,100]';
Signals(1).signal=@signal;
%receivers.  only need to define position
j=1;
while j<=N_R
    Y(:,j)=[(j-1)*100/N_R,1000]';
    j=j+1;
end
%scatterers
Z=zeros(2,K);
Z(:,1)=[0,0]';
Z(:,2)=[-10,0]';
Z(:,3)=[10,0];
%reflectivities
v=[-100,-100,-100];
M=zeros(N_R*N_S,M1*M2);
M_s=zeros(N_R,M1*M2);
%vhat=zeros(M1*M2,1);
I=zeros(M1,M2);
R=zeros(N_R*N_S,1);

%generate data
%generate sampled received data
s=1;
while s<=N_S
    j=1;
    while j<=N_R 
        R((s-1)*N_R+j,1)=0;

        l=1;
        while l<=N_T
            temp=0;
            
            k=1;
            while k<=K            
                temp=temp+w(s)^2*exp(1i*w(s)*(norm(Y(:,j)-Z(:,k))+norm(X(:,l)-Z(:,k)))/c_0)*v(k)/((4*pi)^2*norm(Z(:,k)-Y(:,j))*norm(Z(:,k)-X(:,l)));
                k=k+1; 
            end
            
            R((s-1)*N_R+j,1)=R((s-1)*N_R+j,1)+P(l,s)*temp;
            l=l+1; 
         end
            
        
        R((s-1)*N_R+j,1)=-R((s-1)*N_R+j,1)/(4*pi)^2;
        j=j+1;
    end
    s=s+1;
end
        
%generate sampled signals
s=1;
while s<=N_S
    l=1;
    while l<=N_T
        P(l,s)=Signals(l).signal(w(s));
        l=l+1;
    end
    s=s+1;
end



%generate matrix
s=1;
while s<=N_S
    j=1;
    M_s=zeros(N_R,M1*M2);
    
    while j<=N_R
        m1=1;
        while m1<=M1
            m2=1;
            while m2<=M2
                k=M1*(m2-1)+m1;
                z_k=[x1(m1),x2(m2)]';
                l=1;
                while l<=N_T                    
                M_s(j,k)=M_s(j,k)+P(l,s)*w(s)^2*exp(1i*w(s)*(norm(z_k-X(:,l))+norm(z_k-Y(:,j)))/c_0)/((4*pi)^2*norm(z_k-Y(:,j))*norm(z_k-X(:,l)));
                l=l+1;
                end 
                M_s(j,k)=-M_s(j,k);
                m2=m2+1;
            end
            m1=m1+1;
        end
        j=j+1;
    end
    M(((s-1)*N_R + 1):((s-1)*(N_R)+N_R),:)=M_s;
    s=s+1;
    N_S-s
end

%det(pinv(M)*M)
%%%%testing
M=100*rand(N_S*N_R,M1*M2);

%recover image
vhat=M\R;
vhat=pinv(M)*R;
size(vhat)
m1=1;
while m1<=M1
    m2=1;
    while m2<=M2
        I(m1,m2)=vhat((M1-1)*m2+m1);
        m2=m2+1;
    end
    m1=m1+1;
end



%plot image
imagesc(x1,x2,I);

xlabel('x1 position (meters)');
ylabel('x2 position');



