%define constants and stuff
c_0=3*10^8;
N_T=2;
N_R=2;
K=1;
N_S=800;
B=800;
f0=5500;

w=2*pi*(f0-B/2):2*pi*B/N_S:2*pi*(f0+B/2);
%left corner of image
imageposition=[-5000,-5000]';
%height, width
imagedimensions=[10000,10000]';
M1=18;
M2=18;
deltax1=imagedimensions(1)/M1;
deltax2=imagedimensions(2)/M2;
x1=imageposition(1)+deltax1/2:deltax1:imageposition(1)+ M1*deltax1-deltax1/2;
x2=imageposition(2)+deltax2/2:deltax2:imageposition(2)+M1*deltax2-deltax2/2;

H=zeros(N_R,N_T*N_S);
P=struct('signal',@signal);
Y=zeros(2,N_R);
X=zeros(2,N_T);
s=1;
l=1;
j=1;
%transmitters
while l<=N_T
%      X(:,l)=[1000,1000*(l-N_T/2)]';
        X(:,l)=[(randi(100000)-50000),(randi(100000)-50000)]';
   
     l=l+1;
end;
P(1).signal=@signal;
%receivers.  only need to define position
j=1;
while j<=N_R
     Y(:,j)=[(randi(100000)-50000),(randi(100000)-50000)]';
%     Y(:,j)=[1000*(j-N_R/2),1000]';
    j=j+1;
end
%scatterers
Z=zeros(2,K);
Z(:,1)=[0,0]';
%reflectivities
v=[-100];
H_f=zeros(N_R,N_T);

m1phaseinteger=10;
m2phaseinteger=10;
m1start=m1phaseinteger*M1;
m2start=m2phaseinteger*M2;

VCShat=zeros(N_R,2*N_S*N_T);
%this is our estimated image
vhat=zeros(M1,M2);
XI1i=m1start:1/(M1*deltax1):m1start+(M1-1)/(M1*deltax1);
XI2i=m2start:1/(M2*deltax2):m2start+(M2-1)/(M2*deltax2);
XI1=zeros(N_R,2*N_S*N_T);
XI2=zeros(N_R,2*N_S*N_T);


%generate sampled channel matrix
%go through every sample frequency
s=1;
while s<=N_S
    
    %generate the channel for a fixed frequency.
        j=1;
        while j<=N_R
            l=1;
            while l<=N_T
                jlthentry=0;
                k=1;
                while k<=K
                    jlthentry=jlthentry+exp(-1i*w(s)*(norm(Y(:,j)-Z(:,k))+norm(X(:,l)-Z(:,k)))/c_0)*v(k)/(norm(Y(:,j)-Z(:,k))*norm(X(:,l)-Z(:,k)));
                    k=k+1;
                end
                H_f(j,l)=jlthentry;
                l=l+1;
            end
            j=j+1;
        end
    H_f=-H_f/((w(s)^2)*(4*pi)^2);
    
    H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T))=H_f;
    s=s+1;
end

%estimate image
m1=1;
while m1<=M1
    m2=1;
    while m2<=M2
        s=1;
        while s<=N_S
            %remove constant and use it to get V, then estimate v using sum
            j=1;
            while j<=N_R
                l=1;
                    while l<=N_T
                        H_f=H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T));
                        
                        
                        
                        jlthconstant=H_f(j,l);
                        jlthconstant=conj(jlthconstant)*(4*pi)^2*norm(Y(:,j))*norm(X(:,l))*exp(1i*w(s)*(norm(Y(:,j))+norm(X(:,j))))/(w(s))^2;
                        vhat(m1,m2)=vhat(m1,m2)+jlthconstant*exp(1i*w(s)/c_0*dot((X(:,l)/norm(X(:,l))+Y(:,j)/norm(Y(:,j))),[x1(m1),x2(m2)]'));
                      l=l+1;
                    end
              
                    j=j+1;
            end

           s=s+1;
    
        end
        
     
    
    m2=m2+1;  
    end
    m1=m1+1;
    M1-m1
end


%extract fourier data while using realness property of v
% s=1;
% while s<=N_S
% 	j=1;
% 		while j<=N_R
% 			l=1;
% 			while l<=N_T
% 			
% 			VCShat(j,N_S*N_R+(s-1)*N_T+l)=conj(H(j,(s-1)*N_T+l))*(4*pi)^2*norm(X(l))*norm(Y(j))*exp(1i*w(s)*(norm(X(l))+norm(Y(j))))/(w(s)^2);
%                 
%             VCShat(j,(s-1)*N_T+l)=conj(VCShat(j,N_S*N_R+(s-1)*N_T+l));
%             XI=w(s)*(X(l)/norm(X(l))+receivers(1,j)/norm(receivers(j)))/c_0;
%             XI1(j,(s-1)*N_T+l)=XI(1);
%             XI2(j,(s-1)*N_T+l)=XI(2);
%                 
%             l=l+1;
% 			end
% 			j=j+1;
% 		end
% 		s=s+1;
% end
% 
% 
% %interpolate VCShat to estimate VD    
% %interpolate complex part;
% VDhat=interp2d(XI1,XI2,real(VCShat),XI1i,XI2i)+1i*interp2d(XI1,XI2,imaginary(VCShat),XI1i,XI2i);
% 
% %recover image using 2d ifft on VDhat
% n1=1;
% while n1<M1
%     n2=1;
%     while n2<M2
%         
%         %for each pixel find perform idft
%         m1=1;
%         while m1<M1
%             m2=1;
%             while m2<M2
%                         vhat(n1,n2)=vhat(n1,n2)+VDhat(m1,m2)*exp(2*pi*1i*(n1*m1/M1+n2*m2/M2));
%                 m2=m2+1;
%             end
%            m1=m1+1; 
%         end
%         n2=n2+1;
%     end
%    n1=n1+1; 5
% end




%plot image
imagesc(x1,x2,vhat.*conj(vhat));

xlabel('x1 position (meters)');
ylabel('x2 position');
colorbar;
set(gca,'YDir','normal')