%return a matrix containing sampled transmitted data.  each collumn is the
%recieve vector at a sample frequency
function data=generatedata(transmitters,recievers, N_T, N_R, N_S,start,stop)
    %initialize data matrix
    data=zeros(N_R,N_S);

    %iterate through each sample frequency and fill in the recieve vector
    s=1;
    while s<=N_S
        frequencysamplepoint=start+((stop-start)/N_S)*(s-1);
        %go through each reciever and fill in its recieved value for current
        %frequency
        j=1;
        while j<=N_R
            %fill in value for jth reciever at sth frequency.  this is just the
            %scattered field at that point.
            data(j,s)=pointscatteredfield1d(transmitters,N_T,recievers(j),frequencysamplepoint,start,stop);
            j=j+1;
        end
        s=s+1;
    
        %count down the number of frequencies left to go through
        %'there are only'
        N_S-s
        %'iterations left'
    end
end