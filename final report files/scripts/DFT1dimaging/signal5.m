%simple linear signal.  it is zero at start, and stop at stop.  zero if t is outside of start<t<stop
function y= signal5( w,start, stop )
    if( w<=stop) && (w>=start)
        y=sin(5*w);
    else 
        y=0;
    end
end

