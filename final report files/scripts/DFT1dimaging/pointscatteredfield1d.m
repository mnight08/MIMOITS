function field=pointscatteredfield1d(transmitters,N_T,x,w,start,stop)
    %define scatterers
    %number of scatterers
    K=5;
    %speed of light
    c_0=3*10^8;

    %row vector containing the positions of scatterers along the x1 axis
    positions=[-10, -5, 0, 9,100];
    %reflectivities
    sigma=[-1000,-200, -1000,-200,-5];

    %return the scattereed field
    field=0;
    k=1;
    while k<=K
        field=field + sigma(k)*exp(1i*w*(abs(x-positions(k)))/c_0)*incidentfield1d(transmitters,N_T,positions(k),w,start,stop)/((4*pi)*abs(x-positions(k)));
        k=k+1;
    end
    field=-w^2*field;
end