%take the channel matrix estimates and map them onto a grid for use by
%ifft2.  what we do is get as much fourier data as we can, then we
%interpolate it, and use that to produce the output F.
function F=extractfourierdata1d(transmitters,receivers,N_T,N_R,H,N_S,start,stop,deltax1,M1)
%	F=zeros(2*N_S*N_T*N_R,1);
    F=zeros(N_S*N_T*N_R,1);
	c_0=3*10^8;
    
    
	Y=zeros(N_S*N_T*N_R,1);
	X=zeros(N_S*N_T*N_R,1);

	
    
    
    K=deltax1*start/c_0;
	m=K*M1/(M1*deltax1):1/(M1*deltax1):(K*M1+M1-1)/(M1*deltax1);
	m=-m;
	N=N_S*N_R*N_T;

	
	%take the channel matrix real and imaginary parts and put them in vectors RE,IM along with their position 
	%in fourier space in X.  These will be latter used to interpolate the fourier space
	s=1;
	n=1;
	while s<=N_S
		j=1;
		w=start+((stop-start)/N_S)*(s-1);
		fouriermatrix=scalechannelmatrix(transmitters,receivers,N_T,N_R,H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T)),w);
		while j<=N_R
			l=1;
			while l<=N_T
				X(n)=w*(transmitters(l).position/norm(transmitters(l).position)+receivers(j)/norm(receivers(j)))/c_0;
				Y(n)=fouriermatrix(j,l);
				
				l=l+1;
				n=n+1;
			end
			j=j+1;
		end
		s=s+1;
	end
	X
	Y
    m
	%fill in the upper half of the fourier data with the real and complex parts we interpolated.
	%F((N_S*N_T*N_R+1):2*N_S*N_T*N_R)=interpcomplex1d(X,Y,XI);
	F=interpcomplex1d(X,Y,m);
	%use the fact that the scene is real to double the fourier coverage by making F(-n)=F(n)
    % 	n=1;
    % 	while n<=N
    % 		F(n)=F(n+N)';
    % 	end
	
end