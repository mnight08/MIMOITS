%1-d imaging using channel matrix model

'defining parameters'
clear all
%define physical constants
c_0=3*10^8;
eps=1e-100;



%image center,resolution, width, length, height
center=0;
%pixel spacing
deltax1=1;
%deltax2=.01;
%number of pixels in image
M_1=500;
%M_2=100;

%number of transmitters
N_T=1;
%number of recievers
N_R=1;
%number of scatterers
K=5;
%number of samples
N_S=10000;
%N_S=100;
%N_S=1000;
%N_S=10000;
%N_S=100000;
%N_S=1000000;

'defining transmiters'
%define transmitters
start=10e2;
stop=10e9;
%bandwidth, center frequency, scaling factor for matrix
%B=6.4*10^6;
%f_c=10^8;

transmitters=struct('fouriersignal',@signal,'position',0);
transmitters(1).fouriersignal=@signal1;
transmitters(1).position=-1000;
transmitters(2).fouriersignal=@signal2;
transmitters(2).position=-9000;
transmitters(3).fouriersignal=@signal3;
transmitters(3).position=-8000;
transmitters(4).fouriersignal=@signal4;
transmitters(4).position=-7000;
transmitters(5).fouriersignal=@signal5;
transmitters(5).position=-6000;


'defining recievers'
%define reciever at same position as transmitter
receivers=zeros(N_R,1);
receivers(1)=-1000;
receivers(2)=-9000;
receivers(3)=-8000;
receivers(4)=-7000;
receivers(5)=-6000;

%generate the data
'generating data'
%data=generatedata(transmitters,receivers, N_T, N_R, N_S,start,stop);

%generate what channel matrix H should be.  estimating H seems to be a
%problem.  will use real H to test image inversion.
H=generatechannelmatrix1d(transmitters,receivers,N_T,N_R,N_S,start,stop);


%estimate channel matrix
%'filling matrix'
%Hhat=estimatechannelmatrix1d(transmitters,N_T,N_R,data,N_S,start,stop);



%pull fourier data from real channel matrix
V=extractfourierdata1d(transmitters,receivers,N_T,N_R,H,N_S,start,stop,deltax1,M_1);
size(V);
m=1;
while m<=M_1
   if isnan(V(m))
       V(m)=0;
   end
   m=m+1; 
end
X=-200:deltax1:299;
figure;
plot(X,(ifftshift(ifft(V))).*(conj(ifftshift(ifft(V)))));
%title('Estimating Two Point Scatters: 10 frequency samples');
%title('Estimating Two Point Scatters: 100 frequency samples');
%title('Estimating Two Point Scatters: 1000 frequency samples');
%title('Estimating Two Point Scatters: 10000 frequency samples');
%title('Estimating Two Point Scatters: 100000 frequency samples');
title('Estimating Two Point Scatters: 10000 frequency samples');

xlabel('position (meters)')
ylabel('estimated power of v')
% frequencies=start:(stop-start)/N_S:stop-(stop-start)/N_S;
% s=1;
% signals=zeros(N_T,N_S);
% size(signals)
% size(frequencies)
% while s<=N_S
%     signals(1,s)=transmitters(1).fouriersignal(frequencies(s),start,stop);
%     signals(2,s)=transmitters(2).fouriersignal(frequencies(s),start,stop);
%     signals(3,s)=transmitters(3).fouriersignal(frequencies(s),start,stop);
%     signals(4,s)=transmitters(4).fouriersignal(frequencies(s),start,stop);
%     signals(5,s)=transmitters(5).fouriersignal(frequencies(s),start,stop);
%     s=s+1
% end
% %set(plot(frequencies,signals(1,:)),'color','red');
%plot(frequencies,signals(2,:));
%plot(frequencies,signals(3,:));
%plot(frequencies,signals(4,:));
%plot(frequencies,signals(5,:));
%V=ifft(fftshift(F));

%plot(V);