function sum= scatteredatpoint(x, t, antennas, N,scatterers,Numberofscatterers)
    sum=0;
    c0=3e8;
    i=1;
    while i<=N 
        j=1;
        while j<=Numberofscatterers
        sum = sum+scatterers(j).sigma*signal(t-(norm(x-scatterers(j).pos) + norm(x-antennas(i).pos))/c0,antennas(i).cf)/(4*pi*norm(x-antennas(i).pos)*4*pi*norm(x-scatterers(j).pos)); 
        j=j+1;
        end
       i=i+1;
    end
end