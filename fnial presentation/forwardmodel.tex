
\section{Forward Model}
\begin{frame}{The Wave Equation}
First we introduce some mathematics necessary to describe the physics of RADAR.
The wave equation for $u:\R^d \times \R \to \C$ in free space is given by
\begin{equation}
\left(\laplacian-c_0^{-2}\partial_t^{2}\right)u(x,t)=-j(x,t).
\end{equation}
The Green's function $g_0(x,t)$ for the wave equation in $3$ spatial dimensions given by
\begin{equation}g_0(x,t)=\frac{\delta(t-\frac{|x|}{c_0})}{4 \pi |x|}\end{equation}
can be used to solve for the wave $u(x,t)$ as
\begin{equation}
u(x,t)=\int_{\R^3}\int_{\R}g_0(x-z,t-\tau)  j(z,\tau) \dd \tau \dd z
\end{equation}
\end{frame}

\begin{frame}{Scalar Wave Model}
Under certain assumptions each component of the electric and magnetic vector fields governed by Maxwell's equations can be modeled by the wave equation.
\begin{itemize}
\item We adopt this as our model for the propagation of the scalar electromagnetic wave $u(x,t)$ in $3$-d space with source $j(x,t)$:   \begin{equation} \left(\laplacian - c^{-2}(x)\partial_t^2\right)u(x,t)=-j(x,t). \end{equation}
\item The total field $u(x,t)$ at any position in space $x$ and any instance in time $t$  is decomposed into incident and scattered fields  \begin{equation} u(x,t)=\uin(x,t)+\usc(x,t).\end{equation}
\item The incident field is in the absence of scatterers and satisfies  \begin{equation}  \left(\laplacian-c_0^{-2}\partial_t^2\right)\uin(x,t)=-j(x,t). \end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Reflectivity function}

Define the \textbf{reflectivity} function $v(x)$ as
\begin{equation} \label{eq: ref}
v(x)=\frac{1}{c_0^2}-\frac{1}{c^2(x)}
\end{equation}
\begin{itemize}
\item This function tells us how EM radiation behaves at a point in space compared to in free space.
\item $v(x)$ is $0$ in the absence of targets.
\item In the presence of targets the reflectivity becomes a negative number dependent on the material of the scatterer.
\end{itemize}
\end{frame}




\begin{frame}{Scattered Field}
By subtracting the incident and total field equations we can find that $\usc$ satisfies
\begin{equation}
\left(\laplacian-c_0^{-2}\partial_t^2\right)\usc(x,t)=-v(x)\partial_t^2 u(x,t)
\end{equation}
We can then solve for the scattered field as
\begin{equation}
\usc(x,t) = \int \int  g_0(x-z,t-\tau)\vc(z)\partial_{\tau}^2 u(z,\tau) \dd z \dd \tau
\end{equation}
We can use the solution for the scattered field to find a nonlinear integral equation for the total field.
\begin{equation}
u(x,t) =\uin(x,t) +  \int \int  g_0(x-z,t-\tau)\vc(z)\partial_{\tau}^2 u(z,\tau) \dd z \dd \tau
\end{equation}
\end{frame}





\begin{frame}{Neumann Series}
When the incident field interacts with a target it gets scattered.  This radiation can again get scattered an arbitrary number of times.
The field that has been scattered $k$ times is given by
\begin{equation}
\begin{tabular}{ll}
 u_k=&\int \int g_0(x-z_1,t-\tau_1) v(z_1) \partial_{\tau_1}^2 ...\\
  &\int \int  g_0(z_{k-1}-z_k,\tau_{k-1}-\tau_k) v(z_k) \partial_{\tau_k}^2 \\
  &\uin(z_k,t-\tau_k)\dd z_k \dd \tau_k..\dd z_1 \dd \tau_1.\\
\end{tabular}
\end{equation}\pause
If the field has yet to be scattered, then $k=0$ and
\begin{equation}u_0(x,t)=\uin(x,t)\end{equation}\pause
The total field is then simply the sum of all $u_k$
\begin{equation}
u(x,t)=\lim_{n\rightarrow \infty } \sum^{n}_{k=0}{ u_k }
\end{equation}
This is the solution we find by iterating the recurrence equation for the total field.
\end{frame}


\begin{frame}{Born approximation}
Convergence of this series is dependent on $v$ as well as the incident field. Conditions for rapid convergence include
\begin{itemize}
\item Low reflectivity targets.
\item Well spaced scatterers.
\item Power loss each time the field scatters
\end{itemize}
We take the first two terms in the Neumann series as an approximation to the total field.  This means that only the radiation from the first interaction is non negligible.
\begin{equation}
u(x,t) \approx \uin(x,t) + \int \int g_0(x-z,t-\tau)v(z)\partial_{\tau}^2 \uin(z,\tau)\dd z \dd \tau.
\end{equation}
With this truncation of the series we have that the scattered field is approximated by
\begin{equation}
\usc(x,t) \approx \int \int g_0(x-z,t-\tau)v(z)\partial_{\tau}^2 \uin(z,\tau)\dd z \dd \tau.
\end{equation}
\end{frame}


\begin{frame}{Convolution}
The name of the integral we have been using is called a convolution integral.  We can write this integral succinctly by use of the convolution operator. The convolution operator  in space and time $\convxt$ of $f:\R^d \times \R \to
\C$ and $g:\R^d\times \R \to \C$ is given by
\begin{equation}\label{eq: convxtdef}
f(x,t)\convxt g(x,t) = \int_{\R^d} \int_{\R} f(x-z,t-\tau)g(z,\tau) d \tau \dd z
\end{equation}
Likewise the convolution only in space $\convx$ is given by
\begin{equation} \label{eq: convxdef}
f(x,t)\convx g(x,t) = \int_{\R^d} f(x-z,t)g(z,t)\dd z .
\end{equation}
Using this notation we have that the scattered field satisfies
\begin{equation}
\usc(x,t) \approx g_0(x,t)\convxt v(x)\partial_{t}^2 \uin(x,t).
\end{equation}
\end{frame}


\begin{frame}{Temporal Fourier Transform}
If $u: \R^3 \times \R \to \C$ as $(x,t) \mapsto u(x,t)$ we define the \textbf{temporal Fourier transform} $U(x,f)$ transforming $t \to f$ as
\begin{equation}
U(x,f)=\int u(x,t)e^{-2\pi i tf}\dd t.
\end{equation}
Capital letters will denote Temporal Fourier transforms, i.e.  $F$ will denote the  Temporal Fourier transform of the
function $f$.

Properties of the Temporal Fourier Transform include:
\begin{itemize}
\item Transforms time variable into frequency variable
\item Turns time derivatives into polynomials of new variable
\item Has an inverse given by \\ \begin{equation}u(x,t)=\int U(x,f)e^{2\pi i tf}\dd f.\end{equation}
\end{itemize}


Restrictions of the Temporal Fourier Transform:
\begin{itemize}
\item Function must be compactly supported
\end{itemize}
\end{frame}


\begin{frame}{Convolution Theorem}
The Convolution Theorem relating convolution to multiplication when applying the Temporal Fourier Transform is given by
\begin{equation} \label{eq: convthm}
\int_{\R}f(x,t)\convxt g(x,t)e^{-2\pi i tf}\dd t=F(x,f)\convx G(x,f)
\end{equation}

Going into the frequency domain $g_0(x,t)$, $\uin(x,t)$, $\usc(x,t)$, and $j(x,t)$ become $G_0(x,f)$, $\Uin(x,t)$, $\Usc(x,t)$, and $J(x,t)$.  The transformed incident field satisfies
\begin{equation} \label{eq: Uin}
\left(\laplacian+c_0^{-2}\omega^2\right)\Uin(x,f)=-J(x,f).
\end{equation}
By the convolution theorem the transformed scattered field satisfies
\begin{equation} \label{eq: Usc}
\Usc(x,f) \approx - G_0(x,f)\convx \vc(x) \omega^2 \Uin(x,f)
\end{equation}
where $\omega=2\pi f$ and
\begin{equation}\label{eq: greensfreq}
G_0(x,f)=\frac{e^{-2\pi if|x|/c_0}}{4\pi|x|}.
\end{equation}
\end{frame}

\section{Multiple Input Multiple Output RADAR}





\begin{frame}{Multiple Input Multiple Output - (MIMO) Communication}
\begin{itemize}
\item Communication is about getting a message or signal from one point to another.\pause
\item MIMO Communication is about getting several messages from several points to several other points.\pause
\item Senders send messages.\pause
\item Receivers receive messages.\pause
\item The interface between senders and receivers is called the channel.\pause
\item The goal is to get a message from a particular transmitter to a particular receiver without damaging the message.
\end{itemize}
\end{frame}

\begin{frame}{MIMO Communication}
\begin{figure}\caption{MIMO Communication Diagram}\includegraphics[scale=1.5]{MIMO}\end{figure}
\end{frame}

\begin{frame}{MIMO RADAR}
Utilize multiple transmit and receive antennas to get more information about the scene
\begin{itemize}
\item Apply MIMO communication to RADAR.\pause
\item Don't worry about getting messages perfectly to receivers from transmitters. \pause
\item Focus on deviations from original messages. \pause
\item Get information about scene from how the channel affects messages.
\end{itemize}
\end{frame}

\begin{frame}{MIMO RADAR Transmit}
Each transmitter in a MIMO RADAR system emits its own signal into space which mixes with the signals from the other transmitters.
\includegraphics[scale=.6]{mimoradartransmit}
\end{frame}

\begin{frame}{Isotropic Point Like Transmitters}
The Dirac delta function is the function that satisfies
\begin{equation} \label{equ: deltasupport}
\delta(x)=\left\{0,  x \neq 0\right\}
\end{equation}
\begin{equation} \label{equ: deltaintegrate}
\int{\delta(z)} dz =1.
\end{equation}
\begin{itemize}
\item Zero everywhere except for when its argument is zero.
\item Has equal directional gain.
\item For a smooth function $f:\R^d  \to \C$ \\ \begin{equation}
\int {f(z) \delta(z-x) }\dd z=f(x).
\end{equation}
\end{itemize}
We define a point isotropic antenna located at $z$ as an antenna whose radiation pattern is given by
\begin{equation}
J(x,f)=P_\ell(f)\delta(x-z)
\end{equation}
\end{frame}

\begin{frame}{Incident Field}
We work with MIMO RADAR systems that consist of $N_T$  point like isotropic transmitters located at positions $x_\ell$ and transmitting signals $P_\ell(f)$ so that
$J(x,f)$ is a super position of transmitter signals at their respective locations
\begin{equation} \label{eq: source}
J(x,f)=\sum_{\ell=1}^{N_T} P_\ell(f)\delta(x-x_\ell).
\end{equation}

The incident field is then
\begin{equation} \label{eq: Uinsolve}
\Uin(x,f)=\sum_{\ell=1}^{N_T}P_\ell(f)\frac{e^{-i\omega|x-x_\ell|/c_0}}{4\pi|x-x_\ell|}.
\end{equation}
\end{frame}


\begin{frame}{Example: Incident field for A MIMO Configuration}
\begin{figure}
  \subfloat{\includegraphics[width=0.35\textwidth]{3above3below}}
  \subfloat{\includegraphics[width=0.35\textwidth]{wedge}}
  \caption{Some Antenna Configurations}
\end{figure}
\begin{figure}
  \subfloat{\includegraphics[width=0.28\textwidth]{firstfloor}}
  \subfloat{\includegraphics[width=0.28\textwidth]{secondfloor}}
  \subfloat{\includegraphics[width=0.28\textwidth]{thirdfloor}}
\end{figure}
\end{frame}



\begin{frame}{MIMO RADAR Receiving}
The transmitted field from a MIMO RADAR system interacts with targets and gets scattered.  Each receiver measures the scattered field in some region in space.
\includegraphics[scale=.6]{mimoradarscatter}
\end{frame}


\begin{frame}{Example: Scattered Field for Point Scatterers}
\begin{figure}
  \includegraphics[scale=.2 ]{scattered}
  \caption{Field Scattered By Point Scatterers}
\end{figure}
\end{frame}








